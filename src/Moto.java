public class Moto extends Vehiculo {

    private static final double IMPUESTO = 1.13;
    private String patente;

    public Moto(String color, double precio, String marca, String patente) {
        super(color, precio, marca);
        this.patente = patente;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getPrecioFinal() {
        return IMPUESTO * precio ;
    }

    @Override
    public String getMarca() {
        return marca;
    }
}
