
public class Auto extends Vehiculo {

    private static final double IMPUESTO = 1.30;

    public Auto(String color, double precio, String marca) {
        super(color, precio, marca);
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getPrecioFinal() {
        return IMPUESTO * precio;
    }

    @Override
    public String getMarca() {
        return marca;
    }
}
