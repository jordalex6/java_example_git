
public abstract class Vehiculo {

    String color;
    double precio;
    String marca;

    public Vehiculo(String color, double precio, String marca) {
        this.color = color;
        this.precio = precio;
        this.marca = marca;
    }

    public abstract String getColor();
    public abstract double getPrecioFinal();
    public abstract String getMarca();

}

