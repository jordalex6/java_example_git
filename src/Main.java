public class Main {

    public static void main(String[] args) {

        Vehiculo moto = new Moto("Azul", 25000, "Yamaha", "AFK123");
        Vehiculo auto = new Auto("Rojo", 350000, "Chevrolet");
        Vehiculo bicicleta = new Bicicleta("Blanco", 85000, "Giant");
        Vehiculo camion = new Camion("Blanco", 850000, "Benz");


        System.out.println(String.format("Moto marca %s, color %s. Precio: $%s", moto.getMarca(), moto.getColor(), moto.getPrecioFinal() ));
        System.out.println(String.format("Auto marca %s, color %s. Precio: $%s", auto.getMarca(), auto.getColor(), auto.getPrecioFinal() ));
        System.out.println(String.format("Bicicleta marca %s, color %s. Precio: $%s", bicicleta.getMarca(), bicicleta.getColor(), bicicleta.getPrecioFinal() ));
        System.out.println(String.format("Camion marca %s, color %s. Precio: $%s", camion.getMarca(), camion.getColor(), camion.getPrecioFinal() ));

    }
}
